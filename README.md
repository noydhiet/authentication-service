# Client Authentication Service


[![pipeline status](https://gitlab.mihpccw.com/client-services/authentication-service/badges/master/pipeline.svg)](https://gitlab.mihpccw.com/client-services/authentication-service/commits/master)

Internal Service to provide the feature to get access tokens from the Telkom API when provided PCCW core authentication.