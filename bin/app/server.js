const express = require('express');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const basicAuth = require('../auth/basic_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
const userTokenHandler = require('../modules/user/handlers/user_token_handler');
const swaggerDocument = require('../swagger/swagger.json');

function AppServer() {
  this.server = express();

  this.server.use(cors());

  // required for basic auth
  this.server.use(basicAuth.init());

  this.server.get('/', (req, res) => {
    wrapper.response(res, 'success', wrapper.data('User service'), 'This service is running properly.');
  });

  this.server.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  this.server.get('/user/access-token', basicAuth.isAuthenticated, userTokenHandler.getAccessToken);
}

module.exports = AppServer;
