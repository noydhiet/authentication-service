const rp = require('request-promise');
const validate = require('validate.js');
const moment = require('moment');
moment.locale('id');
const wrapper = require('../../../helpers/utils/wrapper');
const logger = require('../../../helpers/utils/logger');
const config = require('../../../config');
const { InternalServerError } = require('../../../helpers/error');

class UserToken{

  async getAccessToken() {
    const ctx = 'repositories-getAccessToken';
    const options = {
      method: 'GET',
      uri: `${config.get('/indihomeBaseUrl')}/rest/pub/apigateway/jwt/getJsonWebToken?app_id=${config.get('/indihomeAppId')}`,
      headers: {
        'Accept': 'application/json',
        Authorization: config.get('/indihomeBasicAuth'),
      },
      strictSSL: false,
      json: true,
    };

    try {
      const { jwt } = await rp.get(options);
      if (validate.isEmpty(jwt)) {
        const msg = new InternalServerError('Indihome API connection lost');
        logger.log(ctx, msg, 'connection lost');
        return wrapper.error(msg);
      }
      const token = await this.parseToken(jwt);
      const date = new Date(token.exp * 1000);
      const expireAt = moment(date).format('YYYY/MM/DD HH:mm:ss');
      const result = {
        token: jwt,
        expireAt
      };
      return wrapper.data(result);
    } catch (err) {
      logger.log(ctx, err, 'API connection error');
      return wrapper.error(err);
    }
  }

  async parseToken(jwt) {
    const token = jwt.split('.')[1];
    const buff = Buffer.from(token, 'base64');
    const data = buff.toString('utf-8');
    return JSON.parse(data);
  }

}

module.exports = UserToken;
