const wrapper = require('../../../helpers/utils/wrapper');
const UserToken = require('../repositories/user_token_repositories');

const getAccessToken = async (req, res) => {
  const userToken = new UserToken();
  const getData = async () => await userToken.getAccessToken();
  const sendResponse = async (result) => {
    (result.err) ? wrapper.response(res, 'fail', result, 'Get access token failed')
      : wrapper.response(res, 'success', result, 'Get access token successfull');
  };
  sendResponse(await getData());
};

module.exports = {
  getAccessToken
};
